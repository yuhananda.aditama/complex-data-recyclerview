package id.bootcamp.complexrecyclerview

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import id.bootcamp.complexrecyclerview.adapter.ListAdapter
import id.bootcamp.complexrecyclerview.data.ComplexData
import id.bootcamp.complexrecyclerview.data.Data1
import id.bootcamp.complexrecyclerview.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var viewModel : MainViewModel
    private lateinit var mAdapter : ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        //Obeserver
        viewModel.complexDataLiveData.observe(this){
            if (it != null){
                setView(it)
                viewModel.complexDataLiveData.postValue(null)
            }
        }
        //Trigger untuk ambil data ke repo
        viewModel.getAllDataFromRepository()
    }

    private fun setView(complexDataList: List<ComplexData>) {
        mAdapter = ListAdapter(ArrayList(complexDataList))
        binding.rvItem.adapter = mAdapter
        binding.rvItem.layoutManager = LinearLayoutManager(this)

        //Ngilangin ngeblink
        (binding.rvItem.getItemAnimator() as SimpleItemAnimator).supportsChangeAnimations = false

        //Ketika tombol click dipencet
        mAdapter.onTombolClickListener = object : ListAdapter.OnTombolClickListener{
            override fun onTombolClick(data1: Data1) {
                Toast.makeText(this@MainActivity, "Tombol ${data1.name} diclick", Toast.LENGTH_SHORT).show()
            }

        }
    }
}