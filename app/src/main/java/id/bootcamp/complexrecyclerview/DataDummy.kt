package id.bootcamp.complexrecyclerview

import id.bootcamp.complexrecyclerview.data.Data1
import id.bootcamp.complexrecyclerview.data.Data2
import id.bootcamp.complexrecyclerview.data.Data3
import id.bootcamp.complexrecyclerview.data.Data3Inside
import id.bootcamp.complexrecyclerview.data.Data4
import id.bootcamp.complexrecyclerview.data.Data4Inside

//Yang ini gk perlu, kalian nanti ambil datanya dari repository ya
object DataDummy {
    fun getData1DummyList(): List<Data1> {
        val dataList = ArrayList<Data1>()

        for (i in 0..2) {
            val data = Data1()
            data.name = "RS Bersama $i"
            data.location = "CondongCatur $i, Sleman"
            data.address = "Jalan Prayan No.$i, Sinduadi"
            data.price = (i + 1) * 20000L
            data.dataList = arrayListOf(
                "Senin, 0$i:00 - 0${(i+3)}:00",
                "Selasa, 10:00 - 15:00",
                "Rabu, 09:00 - 17:00"
            )
            dataList.add(data)
        }
        return dataList
    }

    fun getData2Dummy(): Data2 {
        val data2 = Data2()
        val dataList = ArrayList<String>()

        for (i in 0..5) {
            dataList.add("Keahlian dan Penanganan $i")
        }

        data2.nameList = dataList
        return data2
    }

    fun getData3Dummy(): Data3 {
        val data3 = Data3()
        val dataList = ArrayList<Data3Inside>()

        for (i in 0..5) {
            val data3Inside = Data3Inside()
            data3Inside.name = "Name $i"
            data3Inside.job = "Job $i"
            data3Inside.year = "199$i - 200$i"
            dataList.add(data3Inside)
        }

        data3.data3InsideList = dataList
        return data3
    }

    fun getData4Dummy(): Data4 {
        val data4 = Data4()
        val dataList = ArrayList<Data4Inside>()

        for (i in 0..5) {
            val data3Inside = Data4Inside()
            data3Inside.name = "Name $i"
            data3Inside.job = "Job $i"
            data3Inside.year = "199$i - 200$i"
            dataList.add(data3Inside)
        }

        data4.data4InsideList = dataList
        return data4
    }
}