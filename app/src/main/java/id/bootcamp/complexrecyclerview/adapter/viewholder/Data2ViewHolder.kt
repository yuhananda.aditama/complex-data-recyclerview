package id.bootcamp.complexrecyclerview.adapter.viewholder

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.bootcamp.complexrecyclerview.R
import id.bootcamp.complexrecyclerview.adapter.ListAdapter
import id.bootcamp.complexrecyclerview.data.ComplexData
import id.bootcamp.complexrecyclerview.data.Data1
import id.bootcamp.complexrecyclerview.data.Data2

class Data2ViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {
    //Biasanya kita gabungin di adapter, tp jika di adapter terlalu rame, kita bisa pisahin viewholdernya
    var layoutDataList = itemView.findViewById<LinearLayout>(R.id.layoutDataListItem)
    var garis = itemView.findViewById<View>(R.id.viewGaris)
    var bottomLayout = itemView.findViewById<LinearLayout>(R.id.bottomLayout)
    var imgExpandedCollapse = itemView.findViewById<ImageView>(R.id.imgExpandCollapse)

    fun aturViewData2Bro(adapter: ListAdapter, listComplexData: ArrayList<ComplexData>, position: Int) {
        val complexData = listComplexData[position]
        val data2 = complexData.data2 as Data2

        //Atur data item yang lebih dari 1
        layoutDataList.removeAllViews()
        for (i in 0..data2.nameList.lastIndex) {
            val textView = TextView(context)
            textView.text = "- ${data2.nameList[i]}"
            layoutDataList.addView(textView)
        }

        //Atur expanded dan collapsenya
        if (data2.isExpanded) {
            garis.visibility = View.VISIBLE
            bottomLayout.visibility = View.VISIBLE
            imgExpandedCollapse.setImageResource(R.drawable.baseline_keyboard_arrow_up_24)
        } else {
            garis.visibility = View.GONE
            bottomLayout.visibility = View.GONE
            imgExpandedCollapse.setImageResource(R.drawable.baseline_keyboard_arrow_down_24)
        }

        //Atur ketika tombol expand/collapse di click
        imgExpandedCollapse.setOnClickListener {
            data2.isExpanded = !data2.isExpanded
            complexData.data2 = data2
            listComplexData.set(position,complexData)
            adapter.notifyItemChanged(position)
        }
    }

}