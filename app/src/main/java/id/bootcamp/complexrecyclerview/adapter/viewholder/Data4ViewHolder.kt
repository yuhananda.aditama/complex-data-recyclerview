package id.bootcamp.complexrecyclerview.adapter.viewholder

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.bootcamp.complexrecyclerview.R
import id.bootcamp.complexrecyclerview.adapter.ListAdapter
import id.bootcamp.complexrecyclerview.data.ComplexData
import id.bootcamp.complexrecyclerview.data.Data3
import id.bootcamp.complexrecyclerview.data.Data4
import id.bootcamp.complexrecyclerview.data.Data4Inside

class Data4ViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {
    //Biasanya kita gabungin di adapter, tp jika di adapter terlalu rame, kita bisa pisahin viewholdernya
    var layoutDataList = itemView.findViewById<LinearLayout>(R.id.layoutDataListItem)
    var garis = itemView.findViewById<View>(R.id.viewGaris)
    var bottomLayout = itemView.findViewById<LinearLayout>(R.id.bottomLayout)
    var imgExpandedCollapse = itemView.findViewById<ImageView>(R.id.imgExpandCollapse)

    fun aturViewData4Bro(adapter: ListAdapter, listComplexData: ArrayList<ComplexData>, position: Int) {
        val complexData = listComplexData[position]
        val data4 = complexData.data4 as Data4

        //Atur data item yang lebih dari 1
        layoutDataList.removeAllViews()
        for (i in 0..data4.data4InsideList.lastIndex) {
            tambahLayoutBro(data4.data4InsideList[i])
        }

        //Atur expanded dan collapsenya
        if (data4.isExpanded) {
            garis.visibility = View.VISIBLE
            bottomLayout.visibility = View.VISIBLE
            imgExpandedCollapse.setImageResource(R.drawable.baseline_keyboard_arrow_up_24)
        } else {
            garis.visibility = View.GONE
            bottomLayout.visibility = View.GONE
            imgExpandedCollapse.setImageResource(R.drawable.baseline_keyboard_arrow_down_24)
        }

        //Atur ketika tombol expand/collapse di click
        imgExpandedCollapse.setOnClickListener {
            data4.isExpanded = !data4.isExpanded
            complexData.data4 = data4
            listComplexData.set(position,complexData)
            adapter.notifyItemChanged(position)
        }
    }

    private fun tambahLayoutBro(data4Inside: Data4Inside) {
        val layout =
            LayoutInflater.from(context).inflate(R.layout.item_data4_inside, layoutDataList, false)

        val tvName = layout.findViewById<TextView>(R.id.tvName)
        val tvJob = layout.findViewById<TextView>(R.id.tvJob)
        val tvYear = layout.findViewById<TextView>(R.id.tvYear)

        tvName.text =  data4Inside.name
        tvJob.text = data4Inside.job
        tvYear.text = data4Inside.year

        layoutDataList.addView(layout)
    }
}