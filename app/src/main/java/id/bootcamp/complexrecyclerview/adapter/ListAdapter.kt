package id.bootcamp.complexrecyclerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.bootcamp.complexrecyclerview.R
import id.bootcamp.complexrecyclerview.adapter.viewholder.Data1ViewHolder
import id.bootcamp.complexrecyclerview.adapter.viewholder.Data2ViewHolder
import id.bootcamp.complexrecyclerview.adapter.viewholder.Data3ViewHolder
import id.bootcamp.complexrecyclerview.adapter.viewholder.Data4ViewHolder
import id.bootcamp.complexrecyclerview.data.ComplexData
import id.bootcamp.complexrecyclerview.data.Data1

class ListAdapter(val complexData: ArrayList<ComplexData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var onTombolClickListener : OnTombolClickListener? = null

    interface OnTombolClickListener{
        fun onTombolClick(data1 : Data1)
    }

    companion object {
        //Pembeda antara view satu dan lainnya
        const val VIEW_DATA1 = 1
        const val VIEW_DATA2 = 2
        const val VIEW_DATA3 = 3
        const val VIEW_DATA4 = 4
    }

    //Pilih View berdasarkan data yang diterima
    override fun getItemViewType(position: Int): Int {
        if (complexData[position].data1 != null) {
            return VIEW_DATA1
        } else if (complexData[position].data2 != null) {
            return VIEW_DATA2
        } else if (complexData[position].data3 != null) {
            return VIEW_DATA3
        } else if (complexData[position].data4 != null) {
            return VIEW_DATA4
        }
        return -1
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        //Pilih layout yang berbeda untuk jenis item yang berbeda
        if (viewType == VIEW_DATA1) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_data1, parent, false)
            return Data1ViewHolder(view, parent.context,onTombolClickListener)
        } else if (viewType == VIEW_DATA2) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_data2, parent, false)
            return Data2ViewHolder(view, parent.context)
        } else if (viewType == VIEW_DATA3) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_data3, parent, false)
            return Data3ViewHolder(view, parent.context)
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_data4, parent, false)
            return Data4ViewHolder(view, parent.context)
        }

    }

    override fun getItemCount(): Int {
        return complexData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        //Dicek apakah viewholdernya itu Data1ViewHolder atau yang lainnya.
        //Perintah buat nampilin ini itu kita taruh di viewholder biar gk kebanyakan disini :)
        if (holder is Data1ViewHolder) {
            holder.aturViewData1Bro(this,complexData,position)
        } else if (holder is Data2ViewHolder) {
            holder.aturViewData2Bro(this,complexData,position)
        } else if (holder is Data3ViewHolder) {
            holder.aturViewData3Bro(this,complexData,position)
        } else if (holder is Data4ViewHolder) {
            holder.aturViewData4Bro(this,complexData,position)
        }
    }
}