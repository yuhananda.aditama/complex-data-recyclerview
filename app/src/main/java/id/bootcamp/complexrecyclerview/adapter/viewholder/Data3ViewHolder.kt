package id.bootcamp.complexrecyclerview.adapter.viewholder

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.bootcamp.complexrecyclerview.R
import id.bootcamp.complexrecyclerview.adapter.ListAdapter
import id.bootcamp.complexrecyclerview.data.ComplexData
import id.bootcamp.complexrecyclerview.data.Data2
import id.bootcamp.complexrecyclerview.data.Data3
import id.bootcamp.complexrecyclerview.data.Data3Inside

class Data3ViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {
    //Biasanya kita gabungin di adapter, tp jika di adapter terlalu rame, kita bisa pisahin viewholdernya
    var layoutDataList = itemView.findViewById<LinearLayout>(R.id.layoutDataListItem)
    var garis = itemView.findViewById<View>(R.id.viewGaris)
    var bottomLayout = itemView.findViewById<LinearLayout>(R.id.bottomLayout)
    var imgExpandedCollapse = itemView.findViewById<ImageView>(R.id.imgExpandCollapse)

    fun aturViewData3Bro(adapter: ListAdapter, listComplexData: ArrayList<ComplexData>, position: Int) {
        val complexData = listComplexData[position]
        val data3 = complexData.data3 as Data3

        //Atur data item yang lebih dari 1
        layoutDataList.removeAllViews()
        for (i in 0..data3.data3InsideList.lastIndex) {
            tambahLayoutBro(data3.data3InsideList[i])
        }

        //Atur expanded dan collapsenya
        if (data3.isExpanded) {
            garis.visibility = View.VISIBLE
            bottomLayout.visibility = View.VISIBLE
            imgExpandedCollapse.setImageResource(R.drawable.baseline_keyboard_arrow_up_24)
        } else {
            garis.visibility = View.GONE
            bottomLayout.visibility = View.GONE
            imgExpandedCollapse.setImageResource(R.drawable.baseline_keyboard_arrow_down_24)
        }

        //Atur ketika tombol expand/collapse di click
        imgExpandedCollapse.setOnClickListener {
            data3.isExpanded = !data3.isExpanded
            complexData.data3 = data3
            listComplexData.set(position,complexData)
            adapter.notifyItemChanged(position)
        }


    }

    private fun tambahLayoutBro(data3Inside: Data3Inside) {
        val layout =
            LayoutInflater.from(context).inflate(R.layout.item_data3_inside, layoutDataList, false)

        val tvName = layout.findViewById<TextView>(R.id.tvName)
        val tvJob = layout.findViewById<TextView>(R.id.tvJob)
        val tvYear = layout.findViewById<TextView>(R.id.tvYear)

        tvName.text =  data3Inside.name
        tvJob.text = data3Inside.job
        tvYear.text = data3Inside.year

        layoutDataList.addView(layout)
    }
}