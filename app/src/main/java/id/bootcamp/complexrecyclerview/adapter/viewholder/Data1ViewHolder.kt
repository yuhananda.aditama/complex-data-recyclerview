package id.bootcamp.complexrecyclerview.adapter.viewholder

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.bootcamp.complexrecyclerview.R
import id.bootcamp.complexrecyclerview.adapter.ListAdapter
import id.bootcamp.complexrecyclerview.data.ComplexData
import id.bootcamp.complexrecyclerview.data.Data1

class Data1ViewHolder(
    itemView: View,
    val context: Context,
    val onTombolClickListener: ListAdapter.OnTombolClickListener?
) : RecyclerView.ViewHolder(itemView) {
    //Biasanya kita gabungin di adapter, tp jika di adapter terlalu rame, kita bisa pisahin viewholdernya
    var tvName = itemView.findViewById<TextView>(R.id.tvName)
    var tvLocation = itemView.findViewById<TextView>(R.id.tvLocation)
    var layoutDataList = itemView.findViewById<LinearLayout>(R.id.layoutDataListItem)
    var tvAddress = itemView.findViewById<TextView>(R.id.tvAddress)
    var tvPrice = itemView.findViewById<TextView>(R.id.tvPrice)
    var btnIniTombol = itemView.findViewById<Button>(R.id.btnIniTombol)
    var garis = itemView.findViewById<View>(R.id.viewGaris)
    var bottomLayout = itemView.findViewById<LinearLayout>(R.id.bottomLayout)
    var imgExpandedCollapse = itemView.findViewById<ImageView>(R.id.imgExpandCollapse)

    fun aturViewData1Bro(adapter: ListAdapter, listComplexData: ArrayList<ComplexData>, position: Int) {
        val complexData = listComplexData[position]
        val data1 = complexData.data1 as Data1

        //Atur data tiap Item
        tvName.text = data1.name
        tvLocation.text = data1.location
        tvAddress.text = data1.address
        tvPrice.text = "Mulai dari \n${data1.price}"

        //Atur data item yang lebih dari 1
        layoutDataList.removeAllViews()
        for (i in 0..data1.dataList.lastIndex) {
            val textView = TextView(context)
            textView.text = data1.dataList[i]
            layoutDataList.addView(textView)
        }

        //Atur expanded dan collapsenya
        if (data1.isExpanded) {
            garis.visibility = View.VISIBLE
            bottomLayout.visibility = View.VISIBLE
            imgExpandedCollapse.setImageResource(R.drawable.baseline_keyboard_arrow_up_24)
        } else {
            garis.visibility = View.GONE
            bottomLayout.visibility = View.GONE
            imgExpandedCollapse.setImageResource(R.drawable.baseline_keyboard_arrow_down_24)
        }

        //Atur ketika tombol expand/collapse di click
        imgExpandedCollapse.setOnClickListener {
            data1.isExpanded = !data1.isExpanded
            complexData.data1 = data1
            listComplexData.set(position,complexData)
            adapter.notifyItemChanged(position)
        }

        //Aksi ketika ini tombol diclick
        btnIniTombol.setOnClickListener {
            onTombolClickListener?.onTombolClick(data1)
        }

    }

}