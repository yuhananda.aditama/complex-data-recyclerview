package id.bootcamp.complexrecyclerview.data

class Data3 {
    var data3InsideList : List<Data3Inside> = ArrayList()
    var isExpanded: Boolean = false //Tanda apakah cardnya terbuka atau tidak
}

class Data3Inside{
    var name : String = ""
    var job: String = ""
    var year : String = ""
}