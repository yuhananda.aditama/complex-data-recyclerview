package id.bootcamp.complexrecyclerview.data

class Data1 {
    var name: String = ""
    var location: String = ""
    var dataList : List<String> = ArrayList()
    var address: String = ""
    var price: Long = 0
    var isExpanded: Boolean = false //Tanda apakah cardnya terbuka atau tidak
}