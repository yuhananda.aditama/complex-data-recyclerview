package id.bootcamp.complexrecyclerview.data

class Data4 {
    var data4InsideList : List<Data4Inside> = ArrayList()
    var isExpanded: Boolean = false //Tanda apakah cardnya terbuka atau tidak
}


class Data4Inside{
    var name : String = ""
    var job: String = ""
    var year : String = ""
}