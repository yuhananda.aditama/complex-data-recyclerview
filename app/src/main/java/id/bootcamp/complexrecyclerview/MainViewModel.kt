package id.bootcamp.complexrecyclerview

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.complexrecyclerview.data.ComplexData
import id.bootcamp.complexrecyclerview.data.Data1
import id.bootcamp.complexrecyclerview.data.Data2
import id.bootcamp.complexrecyclerview.data.Data3
import id.bootcamp.complexrecyclerview.data.Data4
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    val complexDataLiveData = MutableLiveData<List<ComplexData>>()

    fun getAllDataFromRepository() = viewModelScope.launch {
        val complexDataList = ArrayList<ComplexData>()

        //Tambahkan data1
        val data1List = getData1FromRepository()
        for (i in 0..data1List.lastIndex){
            val complexData = ComplexData()
            complexData.data1 = data1List[i]
            complexDataList.add(complexData)
        }

        //Tambahkan data2
        val complexData2 = ComplexData()
        complexData2.data2 = getData2FromRepository()
        complexDataList.add(complexData2)

        //tambahkan data3
        val complexData3 = ComplexData()
        complexData3.data3 = getData3FromRepository()
        complexDataList.add(complexData3)

        //tambahkan data4
        val complexData4 = ComplexData()
        complexData4.data4 = getData4FromRepository()
        complexDataList.add(complexData4)

        //tambahkan data lg jika ada view yang berbeda
        //....

        //update live data
        complexDataLiveData.postValue(complexDataList)
    }

    private suspend fun getData1FromRepository() : List<Data1>{
        //Nanti tinggal diganti panggil fungsi dari repository
        return DataDummy.getData1DummyList()
    }

    private suspend fun getData2FromRepository() : Data2 {
        return DataDummy.getData2Dummy()
    }

    private suspend fun getData3FromRepository() : Data3 {
        return DataDummy.getData3Dummy()
    }

    private suspend fun getData4FromRepository() : Data4 {
        return DataDummy.getData4Dummy()
    }

}